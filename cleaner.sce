// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

function perform_clean()
  try
    getversion("scilab");
  catch
    error("Scilab 5.0 or more is required.");
  end

  root_tlbx = get_absolute_file_path('cleaner.sce');

  if isfile(root_tlbx + '/macros/cleanmacros.sce') then
    exec(root_tlbx+'/macros/cleanmacros.sce');
  end

  if isfile(root_tlbx + '/src/cleaner_src.sce') then
    exec(root_tlbx+'/src/cleaner_src.sce');
  end

  if isfile(root_tlbx + "/sci_gateway/cleaner_gateway.sce") then
    exec(root_tlbx + "/sci_gateway/cleaner_gateway.sce");
   end

  if isfile(root_tlbx + "/help/cleaner_help.sce") then
    exec(root_tlbx + "/help/cleaner_help.sce");
  end

  if isfile(root_tlbx + "/loader.sce") then
    mdelete(root_tlbx + "/loader.sce");
  end

  if isfile(root_tlbx + "/unloader.sce") then
    mdelete(root_tlbx + "/unloader.sce");
  end
endfunction

perform_clean();
clear perform_clean;
