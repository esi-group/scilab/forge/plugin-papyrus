// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.scilab.modules.xcos.addon.visitorpattern;

import static java.util.Arrays.asList;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.scilab.modules.graph.ScilabGraphUniqueObject;
import org.scilab.modules.xcos.block.BasicBlock;
import org.scilab.modules.xcos.block.TextBlock;
import org.scilab.modules.xcos.graph.XcosDiagram;
import org.scilab.modules.xcos.graph.SuperBlockDiagram;
import org.scilab.modules.xcos.link.BasicLink;

import org.scilab.modules.xcos.addon.rmi.ComputeInEclipse;
import org.scilab.modules.xcos.addon.dispatcher.XcosDispatcher;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.model.mxGraphModel.Filter;
import com.mxgraph.model.mxICell;

/**
 * Perform a diagram transformation between Scicos and Xcos.
 */
public final class DiagramElement extends AbstractElement<XcosDiagram> 
{
    protected static final List<String> DATA_FIELD_NAMES = asList("diagram", "props", "objs");
    protected static final List<String> DATA_FIELD_NAMES_FULL = asList("diagram", "props", "objs", "version", "contrib");
    
    /**
     * Window properties (scs_m.props.wpar).
     *
     * This property has no impact among simulation
     */

    /**
     * Default constructor
     */
    public DiagramElement() {
    	
    }
    
    /**
     * Clear cell warnings before encoding
     */
    @Override
    public void beforeEncode(XcosDiagram from) 
    {
        if (from.getAsComponent() != null) 
        {
            from.getAsComponent().clearCellOverlays();
        }
        super.beforeEncode(from);
    }

    /**
     * Encode the instance
     *
     * @param from
     *            the source instance
     */
    @Override
    public void encode(XcosDiagram from) throws Exception {
        beforeEncode(from);

        fillParams(from);

        fillObjs(from);

        afterEncode(from);
    }

    /**
     * Fill the params field
     *
     * @param from
     *            the source instance
     */
    private void fillParams(XcosDiagram from) throws Exception 
    {
        final ScicosParametersElement paramsElement = new ScicosParametersElement();
        paramsElement.encode(from.getScicosParameters());
        
        /**
         * 
         */
        Registry reg = LocateRegistry.getRegistry("localhost");
        ComputeInEclipse comp = (ComputeInEclipse) reg.lookup("ComputeInEclipse");

        if (from instanceof SuperBlockDiagram)
        {
        	SuperBlockDiagram superdiagram = (SuperBlockDiagram) from;
        	// add a diagram identification parameter
        	String diagram_uid = superdiagram.getContainer().getId();
        	String diagram_name = superdiagram.getContainer().getInterfaceFunctionName();
        	paramsElement.simulation_parameters.add(0, diagram_name);
        	paramsElement.simulation_parameters.add(1, diagram_uid);
        } else
        {
        	// add a diagram identification parameter
        	String diagram_name = from.getTitle();
        	paramsElement.simulation_parameters.add(0, diagram_name);
        	String diagram_uid = ((mxCell) from.getDefaultParent()).getId();
        	paramsElement.simulation_parameters.add(1, diagram_uid);
        }
        // send action with parameters
        comp.computeFromXcosToEclipse(XcosDispatcher.CREATEPAPYRUSSIMULATIONINFORMATION, paramsElement.simulation_parameters);
    }

    /**
     * Fill the objs field
     *
     * @param from
     *            the source instance
     */
    private void fillObjs(XcosDiagram from) throws Exception 
    {
        final BlockElement blockElement = new BlockElement(from);
        final LinkElement linkElement = new LinkElement(null);

        final List<BasicBlock> blockList = new ArrayList<BasicBlock>();
        final List<BasicLink> linkList = new ArrayList<BasicLink>();

        /*
         * Fill the block and link lists
         */
        final Filter filter = new Filter() 
        {
            @Override
            public boolean filter(Object current) 
            {
                if (current instanceof BasicBlock && !(current instanceof TextBlock)) 
                {
                    filterBlocks(blockList, linkList, (BasicBlock) current);
                } else if (current instanceof BasicLink) 
                {
                    filterLink(linkList, (BasicLink) current);
                }

                return false;
            }

            /**
             * Filter blocks
             *
             * @param blockList
             *            the current block list
             * @param linkList
             *            the current link list
             * @param block
             *            the block to filter
             */
            private void filterBlocks(final List<BasicBlock> blockList, final List<BasicLink> linkList, final BasicBlock block) 
            {
                blockList.add(block);

                //
                // Look inside a Block to see if there is no "AutoLink"
                // Jgraphx will store this link as block's child
                //
                for (int j = 0; j < block.getChildCount(); ++j) 
                {
                    if (block.getChildAt(j) instanceof BasicLink) 
                    {
                        final BasicLink link = (BasicLink) block.getChildAt(j);

                        // do not add the link if not connected
                        if (link.getSource() != null && link.getTarget() != null) 
                        {
                            linkList.add(link);
                        }
                    }
                }
            }

            /**
             * Filter links
             *
             * @param linkList
             *            the current link list
             * @param link
             *            the link to filter
             */
            private void filterLink(final List<BasicLink> linkList, final BasicLink link) 
            {
                // Only add connected links
                final mxICell source = link.getSource();
                final mxICell target = link.getTarget();
                if (source != null && target != null) 
                {
                    linkList.add(link);
                }
            }
        };
        mxGraphModel.filterDescendants(from.getModel(), filter);

        /*
         * Use a predictable block and links order when debug is enable
         */
        if (Logger.getLogger(BlockElement.class.getName()).isLoggable(Level.FINE)) 
        {
            Collections.sort(blockList);
            Collections.sort(linkList, new Comparator<BasicLink>() {
                @Override
                public int compare(BasicLink o1, BasicLink o2) {
                    return ((ScilabGraphUniqueObject) o1.getSource()).compareTo((ScilabGraphUniqueObject) o2.getSource());
                }
            });
        }

        /*
         * Reorder links
         */
        for (int i = 0; i < linkList.size(); ++i) 
        {
            linkList.get(i).setOrdering(i + blockList.size() + 1);
        }

        /*
         * Reorder and encode blocks
         */
        for (int i = 0; i < blockList.size(); ++i) 
        {
            blockList.get(i).setOrdering(i + 1);

            blockElement.encode(blockList.get(i));
        }

        /*
         * Encode links
         */
        for (int i = 0; i < linkList.size(); ++i) 
        {
            linkElement.encode(linkList.get(i));
        }
    }
}
