// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.


package org.scilab.modules.xcos.addon;


import java.util.logging.Level;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import javax.swing.JOptionPane;

import org.scilab.modules.xcos.Xcos;
import org.scilab.modules.xcos.graph.XcosDiagram;

import com.mxgraph.model.mxCell;

import org.scilab.modules.xcos.addon.visitorpattern.*;
import org.scilab.modules.xcos.addon.rmi.*;
import org.scilab.modules.xcos.addon.dispatcher.*;


public class Plugin implements ComputeInXcos
{
	/**
	 * Constants
	 */


	/**
	 * Attributes
	 */
	private static RmiRegistry rr = null;
	private static Registry registry = null;


	/**
	 * Constructors
	 */
	public Plugin ()
	{
		super();
	}


	/**
	 * Private functions
	 */

	/**
	 * Register to the java RMI mecanism to enable computed Xcos action send by Papyrus
	 */
	private static void registerRmi ()
	{
		if (System.getSecurityManager() == null) 
		{
			System.setSecurityManager(new SecurityManager());
		}
		try 
		{
			String name = "ComputeInXcos";
			ComputeInXcos engine = new Plugin();
			ComputeInXcos stub = (ComputeInXcos) UnicastRemoteObject.exportObject(engine, 0);
			registry = LocateRegistry.getRegistry();
			registry.rebind(name, stub);
			XcosLibrary.LOG.info("Java rmi registered");
		} catch (Exception e) 
		{
			XcosLibrary.LOG.log(Level.SEVERE, XcosLibrary.MSG_EXCEPTION, e);
		}
	}

	/**
	 * Set some java system properties to make the java rmi
	 * communication work between Xcos and Papyrus
	 */
	private static void setSystemProperties()
	{
		Properties p4 = new Properties(System.getProperties());
		String agesys_addon = p4.getProperty("agesys.addon", "");
		String policy = "";
		if (agesys_addon.indexOf("\\") > -1)
		{
			policy = "file:/" + agesys_addon + "\\server.policy";
		} else
		{
			policy = "file://" + agesys_addon + "/server.policy";
		}

		Properties p2 = new Properties(System.getProperties());
		p2.setProperty("java.rmi.server.hostname", "localhost");
		System.setProperties(p2);

		Properties p1 = new Properties(System.getProperties());
		p1.setProperty("java.security.policy", 
				policy);
		System.setProperties(p1);
	}

	/**
	 * Map an Xcos diagram. The user is asked to choose a diagram to map.
	 */
	private static void mapXcosDiagram()
	{
		ScilabDirectHandler handler = new ScilabDirectHandler();

		try 
		{
			// choose the diagram to map
			List<XcosDiagram> diagram_list = Xcos.getInstance().openedDiagrams();
			Object[] diagram_array = diagram_list.toArray();
			String [] diagram_array_name = new String [diagram_array.length]; 
			for (int i = 0; i < diagram_array.length; i++)
			{
				diagram_array_name[i] = ((XcosDiagram) diagram_array[i]).getTitle();
			}
			String diagram_selected = (String) JOptionPane.showInputDialog(null, "Please choose the opened diagram to map:", "AgeSys Mapping",
					JOptionPane.QUESTION_MESSAGE, null, diagram_array_name, diagram_array_name[0]);
			int diagram_id = 0;
			for(diagram_id = 0; diagram_id < diagram_array.length; diagram_id++)
			{
				if (diagram_array_name[diagram_id].equals(diagram_selected))
				{
					break;
				}
			}

			// if diagram has already been mapped then ask user what to do
			Registry reg = LocateRegistry.getRegistry("localhost");
			ComputeInEclipse comp = (ComputeInEclipse) reg.lookup("ComputeInEclipse");
			ArrayList<Object> parameters = new ArrayList<Object> ();
			XcosDiagram thediagramtomap = (XcosDiagram) diagram_array[diagram_id];
			String diagram_uid = ((mxCell) thediagramtomap.getDefaultParent()).getId();
			parameters.add(diagram_uid);
			ArrayList<Object> result = comp.computeFromXcosToEclipse(XcosDispatcher.DELETEPAPYRUSBLOCK, parameters);
			if ((result != null) && (result.size() > 0))
			{
				boolean deletion_done = (Boolean) result.get(0);
				if (deletion_done == true)
				{
					// map xcos diagram into papyrus diagram
					handler.writeDiagram((XcosDiagram) diagram_array[diagram_id]);
					XcosLibrary.DialogInformation("Mapping the selected diagram succeeded");
				} else
				{
					XcosLibrary.DialogInformation("Mapping has been cancelled");
				}
			}

		} catch (Exception e)
		{
			XcosLibrary.LOG.log(Level.SEVERE, XcosLibrary.MSG_EXCEPTION, e);
			XcosLibrary.DialogError("Unable to map the selected Xcos diagram");
		}
	}

	/**
	 * Public functions
	 */

	/**
	 * Initialize the Xcos plugin.
	 * This function is called when the toolbox is loaded.
	 */
	public static void init ()
	{
		String sciverbose = System.getenv("SCIVERBOSE");
		if (sciverbose != null)
		{
			XcosLibrary.LOG.setLevel(Level.ALL);
		} else
		{
			XcosLibrary.LOG.setLevel(Level.OFF);
		}
		
		// launch rmiregistry binary
		rr = new RmiRegistry ();
		rr.start();

		// wait a while to make sure rmiregistry is launched
		try {
			Thread.sleep(1000);
		} catch (Exception e)
		{
			XcosLibrary.LOG.log(Level.SEVERE, XcosLibrary.MSG_EXCEPTION, e);
		}

		// 
		setSystemProperties();

		//
		registerRmi();
	}

	/**
	 * Terminate the Xcos plugin.
	 * This function is called when the toolbox is unloaded.
	 */
	public static void term ()
	{
		if (rr != null)
		{
			if (rr.p != null)
			{
				try {
					rr.p.destroy();
					XcosLibrary.LOG.info("Java rmi daemon killed");
				} catch (Exception e)
				{
					XcosLibrary.LOG.log(Level.SEVERE, XcosLibrary.MSG_EXCEPTION, e);
				}
			}
		}
	}

	/**
	 * Sync from Xcos to papyrus.
	 * This action correspond to the user tool menu action.
	 */
	public static void sync ()
	{
		mapXcosDiagram();
	}

	/**
	 * Java rmi interface implementation
	 */
	@Override
	public ArrayList<Object> computeFromEclipseToXcos(String id, ArrayList<Object> parameters) throws RemoteException {
		return XcosDispatcher.dispatch(id, parameters);
	}
}
