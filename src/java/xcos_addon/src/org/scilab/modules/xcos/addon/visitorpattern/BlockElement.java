// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.scilab.modules.xcos.addon.visitorpattern;


import org.scilab.modules.xcos.addon.rmi.ComputeInEclipse;
import org.scilab.modules.xcos.addon.dispatcher.XcosDispatcher;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.scilab.modules.types.ScilabDouble;
import org.scilab.modules.xcos.Xcos;
import org.scilab.modules.xcos.block.BasicBlock;
import org.scilab.modules.xcos.block.SuperBlock;
import org.scilab.modules.xcos.graph.SuperBlockDiagram;
import org.scilab.modules.xcos.graph.XcosDiagram;
import org.scilab.modules.xcos.port.command.CommandPort;
import org.scilab.modules.xcos.port.control.ControlPort;
import org.scilab.modules.xcos.port.input.InputPort;
import org.scilab.modules.xcos.port.output.OutputPort;

import com.mxgraph.model.mxCell;

/**
 * Perform a block transformation between Scicos and Xcos.
 */
public final class BlockElement extends AbstractElement<BasicBlock> 
{
	/**
	 * Constants
	 */
	
	
	/**
	 * Attributes
	 */
    private final BlockModelElement modelElement;
    private final BlockGraphicElement graphicElement;
    public String name;
    public String uid;
    public String diagram_uid;

    /**
     * Default constructor.
     */
    public BlockElement(final XcosDiagram diag) 
    {
        modelElement = new BlockModelElement(diag);
        graphicElement = new BlockGraphicElement(diag);
    }

    /**
     * Clear cell warnings before encoding
     */
    @Override
    public void beforeEncode(BasicBlock from) 
    {
        XcosDiagram graph = from.getParentDiagram();
        if (graph == null) 
        {
            from.setParentDiagram(Xcos.findParent(from));
            graph = from.getParentDiagram();
            Logger.getLogger(BlockElement.class.getName()).finest("Parent diagram was null");
        }
        if (graph.getAsComponent() != null) 
        {
            graph.getAsComponent().removeCellOverlays(from);
        }
    }

    /**
     * Encode the instance into the element
     *
     * @param from
     *            the source instance
     * @param element
     *            the previously allocated element.
     * @return the element parameter
     * @see org.scilab.modules.xcos.io.scicos.Element#encode(java.lang.Object,
     *      org.scilab.modules.types.ScilabType)
     */
    @Override
    public void encode(BasicBlock from) throws Exception
    {
        beforeEncode(from);
        graphicElement.encode(from);
        modelElement.encode(from);

        /*
         * getting block name, uid and container uid
         */
        String interface_function_name = from.getInterfaceFunctionName();
        String id = from.getId();
        this.name = interface_function_name;
        this.uid = id;
        if (!(from.getParentDiagram() instanceof SuperBlockDiagram))
        {
        	this.diagram_uid = ((mxCell) from.getParentDiagram().getDefaultParent()).getId();
        } else
        {
        	SuperBlockDiagram superdiagram = (SuperBlockDiagram) from.getParentDiagram(); 
        	this.diagram_uid = superdiagram.getContainer().getId();
        }
        
        /*
         * Encoding the ports
         */
        final int numberOfPorts = from.getChildCount();
        int nb_data_in = 0;
        int nb_data_out = 0;
        int nb_event_in = 0;
        int nb_event_out = 0;
        for (int i = 0; i < numberOfPorts; i++) {
            final Object instance = from.getChildAt(i);

            if (instance instanceof InputPort) {
                nb_data_in++;
            } else if (instance instanceof OutputPort) {
                nb_data_out++;
            } else if (instance instanceof CommandPort) {
                nb_event_out++;
            } else if (instance instanceof ControlPort) {
                nb_event_in++;
            }
        }
        
        /*
         * Encode rpar
         */
        String rpar = "";
        try {
        	rpar = "" + ((ScilabDouble) from.getIntegerParameters()).getRealPart()[0][0];
        } catch (Exception e)
        {
        	//XcosLibrary.LOG.log(Level.SEVERE, XcosLibrary.MSG_EXCEPTION, e);
        }
        
        /*
         * create the block
         */
        Registry reg = LocateRegistry.getRegistry("localhost");
        ComputeInEclipse comp = (ComputeInEclipse) reg.lookup("ComputeInEclipse");

        // create first item
        ArrayList<Object> block_parameters = new ArrayList<Object> ();
        // name of block
        block_parameters.add(this.name);
        // x position of the block
        block_parameters.add(this.graphicElement.x);
        // y position of the block
        block_parameters.add(this.graphicElement.y);
        // number of data in port
        block_parameters.add(nb_data_in);
        // number of data out port
        block_parameters.add(nb_data_out);
        // number of event in port
        block_parameters.add(nb_event_in);
        // number of event out port
        block_parameters.add(nb_event_out);
        // uid of the block
        block_parameters.add(this.uid);
        // uid of the parent diagram
        block_parameters.add(this.diagram_uid);
        // rpar of the block
        block_parameters.add(rpar);

        // send action with parameters
        comp.computeFromXcosToEclipse(XcosDispatcher.CREATEPAPYRUSBLOCK, block_parameters);
        
        /*
         * In superblock case, visit the superdiagram element
         */
        if (from instanceof SuperBlock)
        {
        	SuperBlock superblock = (SuperBlock) from;
        	SuperBlockDiagram superdiagram = superblock.getChild();
        	
        	final DiagramElement element = new DiagramElement();
            element.encode(superdiagram);
        }

        afterEncode(from);
    }
}
