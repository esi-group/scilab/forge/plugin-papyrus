// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.scilab.modules.xcos.addon.visitorpattern;

import static java.util.Arrays.asList;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.scilab.modules.xcos.block.BasicBlock;
import org.scilab.modules.xcos.graph.SuperBlockDiagram;
import org.scilab.modules.xcos.link.BasicLink;
import org.scilab.modules.xcos.link.commandcontrol.CommandControlLink;
import org.scilab.modules.xcos.port.BasicPort;

import org.scilab.modules.xcos.addon.rmi.ComputeInEclipse;
import org.scilab.modules.xcos.addon.dispatcher.XcosDispatcher;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxICell;
import com.mxgraph.util.mxPoint;


public final class LinkElement extends AbstractElement<BasicLink> 
{
    protected static final List<String> DATA_FIELD_NAMES = asList("Link", "xx", "yy", "id", "thick", "ct", "from", "to");

    /** Mutable field to easily get the data through methods */
    private BasicPort start;
    private BasicPort end;

    /**
     * Default constructor
     *
     * @param blocks
     *            the already decoded blocks
     */
    public LinkElement(Map<Integer, BasicBlock> blocks) 
    {
    	
    }

    /**
     * Encode the instance into the element
     *
     * @param from
     *            the source instance
     */
    @Override
    public void encode(BasicLink from) throws Exception 
    {
        beforeEncode(from);

        start = (BasicPort) from.getSource();
        end = (BasicPort) from.getTarget();

        // non-connected link
        if ((start == null) || (end == null)) 
        {
            return;
        }

        final mxICell srcBlock = start.getParent();
        final mxICell endBlock = end.getParent();

        // connection not valid
        if ((srcBlock == null) || (endBlock == null)) 
        {
            return;
        }
        if (!((srcBlock instanceof BasicBlock) && (endBlock instanceof BasicBlock))) 
        {
            return;
        }

        final mxGeometry srcGeom = start.getGeometry();
        final mxGeometry endGeom = end.getGeometry();

        // xx and yy
        encodePoints(from, srcGeom, endGeom);

        String type_of_link = "";
        String rate = "";
        if (from instanceof CommandControlLink)
        {
        	type_of_link = "e";
        	rate = "Discrete";
        } else
        {
        	rate = "Continuous";
        }
        
        /*
         * create the link
         */
        Registry reg = LocateRegistry.getRegistry("localhost");
        ComputeInEclipse comp = (ComputeInEclipse) reg.lookup("ComputeInEclipse");

        // create first item
        ArrayList<Object> block_parameters = new ArrayList<Object> ();
        // continuous or discrete
        block_parameters.add(rate);
        // uid of the source block
        block_parameters.add(((BasicBlock) srcBlock).getId());
        // name of the source block port
        block_parameters.add(type_of_link + "out" + start.getOrdering());
        // uid of the target block
        block_parameters.add(((BasicBlock) endBlock).getId());
        // name of the target block port
        block_parameters.add(type_of_link + "in" + end.getOrdering());
        BasicBlock ablock =  (BasicBlock) srcBlock;
        // diagram uid
        String diagram_uid = null;
        if (!(ablock.getParentDiagram() instanceof SuperBlockDiagram))
        {
        	diagram_uid = ((mxCell) ablock.getParentDiagram().getDefaultParent()).getId();
        } else
        {
        	SuperBlockDiagram superdiagram = (SuperBlockDiagram) ablock.getParentDiagram(); 
        	diagram_uid = superdiagram.getContainer().getId();
        }
        block_parameters.add(diagram_uid);

        // send action with parameters
        comp.computeFromXcosToEclipse(XcosDispatcher.CREATEPAPYRUSLINK, block_parameters);

        afterEncode(from);
    }

    /**
     * Encode the link points
     *
     * @param from
     *            the link instance
     * @param srcGeom
     *            the source geometry (output port)
     * @param endGeom
     *            the target geometry (input port)
     */
    private void encodePoints(BasicLink from, final mxGeometry srcGeom, final mxGeometry endGeom) 
    {
        final int ptCount = from.getPointCount();

        mxGeometry geometry;
        final double[][] xx = new double[2 + ptCount][1];
        final double[][] yy = new double[2 + ptCount][1];

        /*
         * Start point
         */
        xx[0][0] = srcGeom.getCenterX();
        yy[0][0] = -srcGeom.getCenterY();
        geometry = start.getParent().getGeometry();
        if (geometry != null) 
        {
            xx[0][0] += geometry.getX();
            yy[0][0] -= geometry.getY() - geometry.getHeight();
        }

        /*
         * Control points
         */
        if (ptCount > 0 && from.getGeometry() != null) 
        {
            final List<mxPoint> lnkPoints = from.getGeometry().getPoints();
            for (int i = 0; i < ptCount; i++) 
            {
                xx[1 + i][0] = (lnkPoints.get(i)).getX();
                yy[1 + i][0] = -(lnkPoints.get(i)).getY();
            }
        }

        /*
         * End point
         */
        xx[1 + ptCount][0] = endGeom.getCenterX();
        yy[1 + ptCount][0] = -endGeom.getCenterY();

        geometry = end.getParent().getGeometry();
        if (geometry != null) 
        {
            xx[1 + ptCount][0] += geometry.getX();
            yy[1 + ptCount][0] -= geometry.getY() - geometry.getHeight();
        }

        // encode the link points
    }
}
