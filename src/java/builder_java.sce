// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

function builder_java_src()
  path_src = get_absolute_file_path("builder_java.sce");

  mprintf("   Building Xcos addon\n");
  ierr = exec(path_src + filesep() + "xcos_addon" + filesep() + "builder_gateway_java.sce", 'errcatch', -1);
  if (ierr <> 0)
    error("Building Xcos addon failed");
  end

  mprintf("   Building Papyrus plugin\n");
  // copy addon.jar to plugin directory
  copyfile(path_src + "/../../jar/addon.jar", path_src + "plugins/org.eclipse.papyrus.agesyscilab/xcos_ressources/");

  // clean papyrus plugin
  rep_clean = unix('ant -f ' + path_src + filesep() + 'plugins' + filesep() + 'org.eclipse.papyrus.agesyscilab' + filesep() + 'builder.xml clean');

  // build papyrus plugin
  rep_build = unix('ant -f ' + path_src + filesep() + 'plugins' + filesep() + 'org.eclipse.papyrus.agesyscilab' + filesep() + 'builder.xml build');
  if ((rep_clean <> 0) | (rep_build <> 0))
    error("Building Papyrus plugin failed");
  end

  // create papyrus update site jars
  // copy plugin and feature project to build directory
  mprintf("   Preparing Papyrus plugin update site\n");
  mkdir(path_src + filesep() + "build");
  mkdir(path_src + filesep() + "build" + filesep() + "plugins");
  mkdir(path_src + filesep() + "build" + filesep() + "features")
  copyfile(path_src + filesep() + "plugins", path_src + filesep() + "build" + filesep() + "plugins");
  copyfile(path_src + filesep() + "features", path_src + filesep() + "build" + filesep() + "features");

  // export it to <TOOLBOX_ROOT>/jar/update_site/
  java_dir = jre_path();
  if (getos() ==  "Windows") then
      java_dir = getshortpathname(java_dir);
  end
  java_bin = java_dir + filesep() + "bin" + filesep() + "java";
  eclipse_pde_dir = ls (ECLIPSE_HOME + filesep() + "plugins" + filesep() + "org.eclipse.pde.build_*");
  eclipse_pde_build_xml = eclipse_pde_dir + filesep() + "scripts" + filesep() + "build.xml";

  mprintf("   Generating Papyrus plugin update site\n");
  rep_install = unix(java_bin + " -jar " + ECLIPSE_HOME + "/plugins/org.eclipse.equinox.launcher_*.jar -application org.eclipse.ant.core.antRunner -buildfile " + eclipse_pde_build_xml + " -Dbuilder=" + path_src + " -Declipse_home=" + ECLIPSE_HOME + " -Dcurrent_directory=" + path_src);
  if (rep_install <> 0)
    error("Generating Papyrus plugin update site failed");
  end

  mprintf("   Adding a category to Papyrus plugin update site\n");
  rep_category = unix(java_bin + " -jar " + ECLIPSE_HOME + "/plugins/org.eclipse.equinox.launcher_*.jar -console -consolelog -application org.eclipse.equinox.p2.publisher.CategoryPublisher -metadataRepository file:/" + path_src + "/../../jar/update_site/ -categoryDefinition file:/" + path_src + filesep() + "features" + filesep() + "org.eclipse.papyrus.agesyscilab.feature" + filesep() + "category.xml");
  if (rep_category <> 0)
    error("Adding a category to Papyrus plugin update site failed");
  end

endfunction

builder_java_src();
clear builder_java_src; // remove builder_java_src on stack

