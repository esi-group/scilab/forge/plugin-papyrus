// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.eclipse.papyrus.agesyscilab.actions.distpatcher;


import java.util.ArrayList;

import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.NamedElement;

import org.eclipse.papyrus.agesyscilab.actions.PapyrusLibrary;


public class DeletePapyrusBlock
{
	/**
	 * Constants
	 */
	public static boolean deletion_done;

	
	/**
	 * Attributes
	 */
	
	
	/**
	 * Constructors
	 */
	
	
	/**
	 * Private functions
	 */
	
	/**
	 * Delete an existing mapping
	 * 
	 * @param diagram_uid : the string uid of the EMF container object to delete
	 * @return the status of the deletion given the user choice
	 */
	private static boolean deleteMappingIfExists (String diagram_uid)
	{
		boolean deletion_done = true;
		
		NamedElement container = PapyrusLibrary.getElementByUid(null, diagram_uid);
		
		// if not found, the result is the root container
		if (container != PapyrusLibrary.getRoot())
		{
			// ask user what to do
			String [] choice = new String [2];
			choice[0] = "Delete previous mapping";
			choice[1] = "Cancel mapping";
			int chosen_action = PapyrusLibrary.dialogChoice("A previous mapping has been detected", choice);
			if (chosen_action == 0)
			{
				PapyrusLibrary.deleteModelElement(container);
				deletion_done = true;
			} else
			{
				deletion_done = false;
			}
		}
		
		return deletion_done;
	}
	
	
	/**
	 * Public functions
	 */
	
	/**
	 * Execute the main action of this class
	 * 
	 * @param parameters : the parameters to deal with
	 * @return a boolean indicating if deletion occured or not
	 * @throws Exception
	 */
	public static ArrayList<Object> executeTask(final ArrayList<Object> parameters) throws Exception
	{
		deletion_done = false;
		
		Display.getDefault().syncExec(
				new Runnable() 
				{
					public void run()
					{
						final String diagram_uid = (String) parameters.get(0);

						deletion_done = deleteMappingIfExists(diagram_uid);
					}
				});
		
		ArrayList<Object> result = new ArrayList<Object> ();
		result.add(new Boolean(deletion_done));
		return result;
	}
}
