// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.eclipse.papyrus.agesyscilab.actions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.sasheditor.contentprovider.IPageManager;
import org.eclipse.papyrus.infra.core.sasheditor.editor.ISashWindowsContainer;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.core.services.ServiceNotFoundException;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.core.utils.ServiceUtils;
import org.eclipse.papyrus.uml.diagram.activity.edit.commands.ActivityCreateCommand;
import org.eclipse.papyrus.uml.diagram.activity.edit.commands.ActivityParameterNodeCreateCommand;
import org.eclipse.papyrus.uml.diagram.activity.edit.commands.CallBehaviorActionCreateCommand;
import org.eclipse.papyrus.uml.diagram.activity.edit.commands.CommentCreateCommand;
import org.eclipse.papyrus.uml.diagram.activity.edit.commands.ForkNodeCreateCommand;
import org.eclipse.papyrus.uml.diagram.activity.edit.commands.ObjectFlowCreateCommand;
import org.eclipse.papyrus.uml.diagram.activity.providers.ElementInitializers;
import org.eclipse.papyrus.uml.diagram.common.editparts.NamedElementEditPart;
import org.eclipse.papyrus.uml.diagram.profile.edit.commands.StereotypeCreateCommand;
import org.eclipse.papyrus.uml.service.types.element.UMLElementTypes;
import org.eclipse.papyrus.uml.tools.model.UmlModel;
import org.eclipse.papyrus.views.modelexplorer.NavigatorUtils;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallBehaviorAction;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ForkNode;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.UMLFactory;

import com.google.common.base.Function;
import com.google.common.base.Predicate;

import org.eclipse.papyrus.agesyscilab.actions.distpatcher.*;

import static org.eclipse.papyrus.agesyscilab.Activator.log;


public class PapyrusLibrary
{
	/**
	 * Constants
	 */
	
	
	/**
	 * Attributes
	 */
	
	
	/**
	 * Private static methods
	 */
	
	/**
	 * Create an EMF call-behavior action object
	 * 
	 * @param container : the containing activity
	 * @param behavior : the behavior of the call-behavior action element
	 * @return the EMF call-behavior action object
	 */
	private static Element createCallBehaviorActionUmlInstance(final Activity container, final Behavior behavior)
	{
		Element element = null;
		
		try 
		{
			CreateElementRequest createRequest = new CreateElementRequest(container, UMLElementTypes.CALL_BEHAVIOR_ACTION);
			CallBehaviorActionCreateCommand acc = new CallBehaviorActionCreateCommand(createRequest, null)
			{
				@Override
				protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
					CallBehaviorAction cba = UMLFactory.eINSTANCE.createCallBehaviorAction();
					cba.setBehavior(behavior);
					initAndExecuteEmfCommand(cba);
					ElementInitializers.getInstance().init_CallBehaviorAction_3008(cba);
					((CreateElementRequest)getRequest()).setNewElement(cba);
					return CommandResult.newOKCommandResult(cba);
				}
			};
			acc.execute(null, null);
			CommandResult cr = acc.getCommandResult();
			element = (Element) cr.getReturnValue();
		} catch (Exception e)
		{
			log.error(e);
		}
		
		return element;
	}
	
	/**
	 * Create an EMF fork node object
	 * 
	 * @param container : the containing activity
	 * @return the EMF fork node object
	 */
	private static Element createForkNodeUmlInstance(final Activity container)
	{
		Element element = null;
		
		try 
		{
			CreateElementRequest createRequest = new CreateElementRequest(container, UMLElementTypes.FORK_NODE);
			ForkNodeCreateCommand acc = new ForkNodeCreateCommand(createRequest, null)
			{
				@Override
				protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
					ForkNode newElement = UMLFactory.eINSTANCE.createForkNode();
					container.getOwnedNodes().add(newElement);
					ElementInitializers.getInstance().init_ForkNode_3040(newElement);
					((CreateElementRequest)getRequest()).setNewElement(newElement);
					return CommandResult.newOKCommandResult(newElement);
				}
			};
			acc.execute(null, null);
			CommandResult cr = acc.getCommandResult();
			element = (Element) cr.getReturnValue();
		} catch (Exception e)
		{
			log.error(e);
		}
		
		return element;
	}
	
	/**
	 * Create an EMF activity parameter node object
	 * 
	 * @param container : the containing activity
	 * @param parameter : the EMF parameter object related to the activity parameter node
	 * @return the EMF activity parameter node object
	 */
	private static Element createActivityParameterNodeUmlInstance(final Activity container, final Parameter parameter)
	{
		Element element = null;
		
		try 
		{
			CreateElementRequest createRequest = new CreateElementRequest(container, UMLElementTypes.ACTIVITY_PARAMETER_NODE);
			ActivityParameterNodeCreateCommand acc = new ActivityParameterNodeCreateCommand(createRequest, null)
			{
				@Override
				protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
					ActivityParameterNode apn = UMLFactory.eINSTANCE.createActivityParameterNode();
					container.getOwnedNodes().add(apn);
					
					apn.setIsControlType(true);
					ElementInitializers.getInstance().init_ActivityParameterNode_3059(apn);
					
					((CreateElementRequest)getRequest()).setNewElement(apn);
					return CommandResult.newOKCommandResult(apn);
				}
			};
			acc.execute(null, null);
			CommandResult cr = acc.getCommandResult();
			element = (Element) cr.getReturnValue();
		} catch (Exception e)
		{
			log.error(e);
		}
		
		return element;
	}
	
	/**
	 * Create an EMF parameter object
	 * 
	 * @param container : The containing behavior
	 * @return the EMF Parameter object
	 */
	private static Element createParameterUmlInstance(final Behavior container)
	{
		Element element = null;
		
		try {
			CreateElementRequest createRequest = new CreateElementRequest(container, UMLElementTypes.ACTIVITY_PARAMETER_NODE);
			ActivityParameterNodeCreateCommand acc = new ActivityParameterNodeCreateCommand(createRequest, null)
			{
				@Override
				protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
					Parameter parameter = UMLFactory.eINSTANCE.createParameter();
					container.getOwnedParameters().add(parameter);
					ElementInitializers.getInstance().init_Parameter_3001(parameter);
					((CreateElementRequest)getRequest()).setNewElement(parameter);
					return CommandResult.newOKCommandResult(parameter);
				}
			};
			acc.execute(null, null);
			CommandResult cr = acc.getCommandResult();
			element = (Element) cr.getReturnValue();
		} catch (Exception e)
		{
			log.error(e);
		}
		
		return element;
	}
	
	/**
	 * Create an EMF object flow object 
	 * 
	 * @param container : the containing activity
	 * @param source : the source of the object flow to connect to
	 * @param target : the target of the object flow to connect to
	 * @return the EMF object flow object
	 */
	private static Element createObjectFlowUmlInstance(final Activity container, final EObject source, final EObject target)
	{
		Element element = null;
		
		try 
		{
			TransactionalEditingDomain editingDomain = ServiceUtils.getInstance().getTransactionalEditingDomain(getServiceRegistry());
			CreateRelationshipRequest createRequest = new CreateRelationshipRequest(editingDomain, UMLElementTypes.OBJECT_FLOW);
			ObjectFlowCreateCommand acc = new ObjectFlowCreateCommand(createRequest, source, target)
			{
				@Override
				protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
					if(!canExecute()) {
						throw new ExecutionException("Invalid arguments in create link command"); //$NON-NLS-1$
					}

					ObjectFlow newElement = UMLFactory.eINSTANCE.createObjectFlow();
					container.getEdges().add(newElement);
					newElement.setSource(getSource());
					newElement.setTarget(getTarget());
					ElementInitializers.getInstance().init_ObjectFlow_4003(newElement);
					((CreateElementRequest)getRequest()).setNewElement(newElement);
					return CommandResult.newOKCommandResult(newElement);
				}
			};
			acc.execute(null, null);
			CommandResult cr = acc.getCommandResult();
			element = (Element) cr.getReturnValue();
		} catch (Exception e)
		{
			log.error(e);
		}
		
		return element;
	}
	
	/**
	 * Create an EMF stereotype object
	 * 
	 * @param objectflow : the object flow to apply the stereotype
	 * @return the EMF stereotype object
	 */
	private static Element createStereotypeUmlInstance(final ObjectFlow objectflow)
	{
		Element element = null;
		
		try 
		{
			CreateElementRequest createRequest = new CreateElementRequest(objectflow, UMLElementTypes.STEREOTYPE);
			StereotypeCreateCommand acc = new StereotypeCreateCommand(createRequest, null)
			{
				@Override
				protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
					Stereotype newElement = UMLFactory.eINSTANCE.createStereotype();
					
					objectflow.getAppliedStereotypes().add(newElement);

					org.eclipse.papyrus.uml.diagram.profile.providers.ElementInitializers.getInstance().init_Stereotype_1026(newElement);

					((CreateElementRequest)getRequest()).setNewElement(newElement);
					return CommandResult.newOKCommandResult(newElement);
				}
			};
			acc.execute(null, null);
			CommandResult cr = acc.getCommandResult();
			element = (Element) cr.getReturnValue();
		} catch (Exception e)
		{
			log.error(e);
		}
		
		return element;
	}
	
	/**
	 * Create an EMF activity object
	 * 
	 * @param container : the containing EMF object
	 * @return the EMF activity object
	 */
	private static Element createActivityUmlInstance(final NamedElement container)
	{
		Element element = null;
		
		try 
		{
			CreateElementRequest createRequest = new CreateElementRequest(container, UMLElementTypes.ACTIVITY);
			ActivityCreateCommand acc = new ActivityCreateCommand(createRequest, null)
			{
				@Override
				protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
					Activity newElement = UMLFactory.eINSTANCE.createActivity();
					if (container instanceof org.eclipse.uml2.uml.Package)
					{
						org.eclipse.uml2.uml.Package package_container = (org.eclipse.uml2.uml.Package) container;
						package_container.getPackagedElements().add(newElement);
					} else
					{
						Activity activity_container = (Activity) container;
						activity_container.getOwnedBehaviors().add(newElement);
					}
					ElementInitializers.getInstance().init_Activity_2001(newElement);
					((CreateElementRequest)getRequest()).setNewElement(newElement);
					return CommandResult.newOKCommandResult(newElement);
				}
			};
			acc.execute(null, null);
			CommandResult cr = acc.getCommandResult();
			element = (Element) cr.getReturnValue();
		} catch (Exception e)
		{
			log.error(e);
		}
		
		return element;
	}
	
	/**
	 * Create an EMF comment object
	 * 
	 * @param container : the containing EMF object
	 * @return the EMF comment object
	 */
	private static Element createCommentUmlInstance(final NamedElement container)
	{
		Element element = null;
		
		try {
			CreateElementRequest createRequest = new CreateElementRequest(container, UMLElementTypes.COMMENT);
			CommentCreateCommand acc = new CommentCreateCommand(createRequest, null)
			{
				@Override
				protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
					Comment newElement = UMLFactory.eINSTANCE.createComment();
					ElementInitializers.getInstance().init_Comment_3080(newElement);
					((CreateElementRequest)getRequest()).setNewElement(newElement);
					return CommandResult.newOKCommandResult(newElement);
				}
			};
			acc.execute(null, null);
			CommandResult cr = acc.getCommandResult();
			element = (Element) cr.getReturnValue();
		} catch (Exception e)
		{
			log.error(e);
		}
		
		return element;
	}
	
	/**
	 * Public static methods
	 */
	
	/**
	 * Set the name of an EMF object
	 * 
	 * @param element : the element to set the label
	 * @param name : the label to set
	 */
	public static void modifyNameFromNamedElement(final NamedElement element, final String name)
	{
		try 
		{
			TransactionalEditingDomain editingDomain = ServiceUtils.getInstance().getTransactionalEditingDomain(PapyrusLibrary.getServiceRegistry());
			CommandStack stack = editingDomain.getCommandStack();
			RecordingCommand rc = new RecordingCommand(editingDomain)
			{
				@Override
				protected void doExecute() {
					element.setName(name);
				}
			};
			stack.execute(rc);
		} catch (Exception e)
		{
			log.error(e);
		}
	}
	
	/**
	 * Populate recursively a list of all the children of an EditPart object
	 * 
	 * @param list : the list to populate
	 * @param root : the root editpart
	 */
	@SuppressWarnings("unchecked")
	public static void findAllChildren(List<EditPart> list, EditPart root) {
		list.addAll(root.getChildren());
		for(Object editPart : root.getChildren()) {
			if(editPart instanceof EditPart) {
				findAllChildren(list, (EditPart)editPart);
			}
		}
	}
	
	/**
	 * Tricky function taken from the Papyrus source code
	 * 
	 * @param context
	 * @return
	 */
	public static Collection<org.eclipse.gmf.runtime.notation.Diagram> evaluate(final Element context)
	{
		Predicate<EStructuralFeature.Setting> p = new Predicate<EStructuralFeature.Setting>() 
		{
			public boolean apply(EStructuralFeature.Setting arg0) 
			{
				return arg0.getEObject() instanceof Diagram ;
			}
		};
		
		Function<EStructuralFeature.Setting, Diagram> f = new Function<EStructuralFeature.Setting, Diagram>() 
		{
			public Diagram apply(EStructuralFeature.Setting arg0) 
			{
				return (Diagram)arg0.getEObject();
			}
		};
		
		return NavigatorUtils.findFilterAndApply(context, p, f);
	}
	
	/**
	 * Open a diagram in the current active editor
	 * 
	 * @param diagram : the diagram to open
	 */
	public static void openDiagram(final Diagram diagram)
	{
		try 
		{
			IEditorPart active_editor = getISashWindowsContainer().getActiveEditor();

			if (!active_editor.getTitle().equals(diagram.getName()))
			{
				for (Object id : getIPageManager().allPages()) {
					DiagramImpl page = (DiagramImpl) id;
					if (page.getName().equals(diagram.getName())) {
						/*
						 * Set the focus of the interface diagram
						 */
						getIPageManager().selectPage(id);
					}
				}
			}
		} catch (Exception e)
		{
			log.error(e);
		}
	}
	
	/**
	 * show the GMF part of an EMF object
	 * 
	 * @param elementToShow : the EMF object to show
	 * @param activeEditor : the active editor
	 * @param editPart : the editpart of the editor
	 * @param x : the position x where to show the element
	 * @param y : the position y where to show the element
	 * @return the updated EditPart corresponding to the EMF elementToShow object
	 */
	public static EditPart showElementIn(EObject elementToShow, DiagramEditor activeEditor, EditPart editPart, int x, int y) 
	{
		EditPart returnEditPart = null;

		if(elementToShow instanceof Element) 
		{
			DropObjectsRequest dropObjectsRequest = new DropObjectsRequest();
			ArrayList<Element> list = new ArrayList<Element>();
			list.add((Element) elementToShow);
			dropObjectsRequest.setObjects(list);
			dropObjectsRequest.setLocation(new Point(x, y));
			org.eclipse.gef.commands.Command commandDrop = editPart.getCommand(dropObjectsRequest);

			boolean processChildren = false;
			if(commandDrop == null) 
			{
				processChildren = true;
			} else 
			{
				if(commandDrop.canExecute()) 
				{
					activeEditor.getDiagramEditDomain().getDiagramCommandStack().execute(commandDrop);
					
					ArrayList<EditPart> children_parts = new ArrayList<EditPart> ();
					findAllChildren(children_parts, (EditPart) editPart);
					for(EditPart item : children_parts)
					{
						if (item instanceof NamedElementEditPart)
						{
							NamedElementEditPart namedelement_part = (NamedElementEditPart) item;
							if (namedelement_part.getNamedElement() == elementToShow)
							{
								returnEditPart = namedelement_part;
								break;
							}
						}
					}
				} else {
					processChildren = true;
				}
			}

			if(processChildren) 
			{
				//try to add to one of its children
				boolean found = false;

				ArrayList<EditPart> childrenList = new ArrayList<EditPart>();
				findAllChildren(childrenList, editPart);
				for(Object child : childrenList) 
				{
					if(child instanceof EditPart) 
					{
						org.eclipse.gef.commands.Command commandDropChild = ((EditPart)child).getCommand(dropObjectsRequest);
						if(commandDropChild != null) 
						{
							if(commandDropChild.canExecute()) 
							{
								activeEditor.getDiagramEditDomain().getDiagramCommandStack().execute(commandDropChild);
								found = true;
								
								ArrayList<EditPart> children_parts = new ArrayList<EditPart> ();
								findAllChildren(children_parts, (EditPart) child);
								for(EditPart item : children_parts)
								{
									if (item instanceof NamedElementEditPart)
									{
										NamedElementEditPart namedelement_part = (NamedElementEditPart) item;
										if (namedelement_part.getNamedElement() == elementToShow)
										{
											returnEditPart = namedelement_part;
											break;
										}
									}
								}
								break;
							}
						}
					}
				}
				
				if(!found) 
				{
					returnEditPart = editPart;
				}
			}
		}

		return returnEditPart;
	}
	
	/**
	 * create an EMF object given some parameters
	 * 
	 * @param container : the containing object
	 * @param uml_type : the type of SysML object to create
	 * @param param : an EMF object argument
	 * @return the EMF create object
	 */
	public static Element createSysmlElementFromUmlType(NamedElement container, IElementType uml_type, NamedElement param) 
	{
		Element newElement = null;
		
		if (uml_type ==  UMLElementTypes.CALL_BEHAVIOR_ACTION)
		{
			newElement = createCallBehaviorActionUmlInstance((Activity) container, (Behavior) param);
		} else if (uml_type ==  UMLElementTypes.ACTIVITY)
		{
			newElement = createActivityUmlInstance(container);
		} else if (uml_type ==  UMLElementTypes.ACTIVITY_PARAMETER_NODE)
		{
			newElement = createActivityParameterNodeUmlInstance((Activity) container, (Parameter) param);
		} else if (uml_type ==  UMLElementTypes.PARAMETER)
		{
			newElement = createParameterUmlInstance((Behavior) container);
		} else if (uml_type == UMLElementTypes.COMMENT)
		{
			newElement = createCommentUmlInstance(container);
		}  else if (uml_type == UMLElementTypes.FORK_NODE)
		{
			newElement = createForkNodeUmlInstance((Activity) container);
		} else
		{
			log.info("\tUnknown uml element to create");
		}

		return newElement;
	}
	
	/**
	 * Create an EMF object given some parameters
	 * 
	 * @param container : the containing EMF object
	 * @param uml_type : the type of EMF object to create
	 * @param source : an argument used in object flows creation
	 * @param target : an argument used in object flows creation
	 * @return the EMF create object
	 */
	public static Element createSysmlElementFromUmlType(Element container, IElementType uml_type, NamedElement source, NamedElement target) 
	{
		Element newElement = null;
		
		if (uml_type ==  UMLElementTypes.OBJECT_FLOW)
		{
			newElement = createObjectFlowUmlInstance((Activity) container, source, target);
		} else if (uml_type == UMLElementTypes.STEREOTYPE)
		{
			newElement = createStereotypeUmlInstance((ObjectFlow) container);
		} else
		{
			log.info("\tUnknown uml element to create");
		}

		return newElement;
	}
	
	/**
	 * Get the EMF object from an UID string
	 * 
	 * @param container : the container to search in
	 * @param uid : the string uid
	 * @return the found EMF object 
	 */
	public static NamedElement getElementByUid(NamedElement container, String uid)
	{
		NamedElement e = null;
		
		if (container == null)
		{
			org.eclipse.uml2.uml.Package package_container = null;
			try 
			{
				container = (org.eclipse.uml2.uml.Package) getRoot();
				package_container = (org.eclipse.uml2.uml.Package) container;
			} catch (Exception exception)
			{
				log.error(exception);
			}

			List<PackageableElement> element_list = package_container.getPackagedElements();
			for (PackageableElement element : element_list)
			{
				if (element instanceof Activity)
				{
					// recursively explore superblock
					Activity block = (Activity) element;
					e = getElementByUid(block, uid);

					// when found, stop the exploration
					if (e != null)
					{
						break;
					}
					
					// get the element if concerned by the uid
					String block_uid = null;
					EAnnotation annotation = block.getEAnnotation(CreatePapyrusSimulationInformation.UID_SOURCE);
					if (annotation != null)
					{
						EList<EObject> eobject_list = annotation.getContents();
						for (EObject eo : eobject_list)
						{
							if (eo instanceof Comment)
							{
								Comment c = (Comment) eo;
								block_uid = c.getBody();
							}
						}
					}
					if (uid.equals(block_uid))
					{
						e = block;
					}

					// when found, stop the exploration
					if (e != null)
					{
						break;
					}
				}
			}
			
			// if not found, use the container instead
			if (e == null)
			{
				e = container;
			}

		} else
		{
			Activity activity_container = (Activity) container;
			List<Behavior> element_list = activity_container.getOwnedBehaviors();
			for (Behavior behave : element_list)
			{
				if (behave instanceof Activity)
				{
					// recursively explore superblock
					Activity block = (Activity) behave;
					e = getElementByUid(block, uid);
					
					// when found, stop the exploration
					if (e != null)
					{
						break;
					}

					// get the element if concerned by the uid
					String block_uid = null;
					EAnnotation annotation = block.getEAnnotation(CreatePapyrusSimulationInformation.UID_SOURCE);
					if (annotation != null)
					{
						EList<EObject> eobject_list = annotation.getContents();
						for (EObject eo : eobject_list)
						{
							if (eo instanceof Comment)
							{
								Comment c = (Comment) eo;
								block_uid = c.getBody();
							}
						}
					}
					if (uid.equals(block_uid))
					{
						e = block;
					}

					// when found, stop the exploration
					if (e != null)
					{
						break;
					}
				}
			}
		}
		
		return e;
	}
	
	/**
	 * Open a dialog box on error
	 * 
	 * @param str : the message to print
	 */
	public static void dialogError(String str)
	{
		MessageDialog.openError(Display.getCurrent().getActiveShell(), "Error", str);
	}
	
	/**
	 * Open a information dialog box
	 * 
	 * @param str : the message to print
	 */
	public static void dialogInformation(String str)
	{
		MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Info", str);
	}
	
	/**
	 * Open a choice dialog box
	 * 
	 * @param message : the message to print
	 * @param choice : an array of the different choices
	 * @return : the index of the choice made by the user
	 */
	public static int dialogChoice(String message, String [] choice)
	{
		MessageDialog dialog = new MessageDialog(Display.getCurrent().getActiveShell(), "Warning", null, message,
				MessageDialog.WARNING, choice, 0);
		dialog.open();
		return dialog.getReturnCode();
	}
	
	/**
	 * Get the service registry object 
	 * 
	 * @return the service registry object
	 * @throws ServiceException
	 */
	public static ServicesRegistry getServiceRegistry() throws ServiceException
	{
		IEditorPart editor;
		try {
			editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
			ServicesRegistry serviceRegistry = (ServicesRegistry)editor.getAdapter(ServicesRegistry.class);
			if(serviceRegistry != null) {
				return serviceRegistry;
			}
		} catch (NullPointerException e) {
			// Can't get the active editor
			throw new ServiceNotFoundException("Can't get the current Eclipse Active Editor. No ServiceRegistry found.");
		}


		// Not found
		throw new ServiceNotFoundException("Can't get the ServiceRegistry from current Eclipse Active Editor");
	}
	
	/**
	 * Get the ISashWindowsContainer object
	 * 
	 * @return this object
	 */
	public static ISashWindowsContainer getISashWindowsContainer()
	{
		ISashWindowsContainer sashContainer = null;
		
		try 
		{
			sashContainer = getServiceRegistry().getService(ISashWindowsContainer.class);
		} catch (Exception e)
		{
			log.error(e);
		}
		
		return sashContainer;
	}
	
	/**
	 * Get the IPageManager object
	 * 
	 * @return this object
	 * @throws ServiceException
	 */
	public static IPageManager getIPageManager() throws ServiceException
	{
		IPageManager pageMngr = null;
		
		pageMngr = getServiceRegistry().getService(IPageManager.class);
		
		return pageMngr;
	}
	
	/**
	 * Get the root EMF object of a SysML project
	 * 
	 * @return the root EMF object
	 */
	public static EObject getRoot() {
		EObject root = null;

		try {
			UmlModel openedModel = (UmlModel)getServiceRegistry().getService(ModelSet.class).getModel(UmlModel.MODEL_ID);
			if(openedModel != null) {
				root = openedModel.lookupRoot();
			}
		} catch (Exception e) {
			log.error(e);
		}

		return root;
	}
	
	/**
	 * Delete an EMF object
	 * 
	 * @param el : the EMF object to destroy
	 */
	public static void deleteModelElement(final Element el)
	{
		try 
		{
			TransactionalEditingDomain editingDomain = ServiceUtils.getInstance().getTransactionalEditingDomain(PapyrusLibrary.getServiceRegistry());
			CommandStack stack = editingDomain.getCommandStack();
			RecordingCommand rc = new RecordingCommand(editingDomain)
			{
				@Override
				protected void doExecute() {
					EcoreUtil.delete(el, true);
				}
			};
			stack.execute(rc);
		} catch (Exception e)
		{
			log.error(e);
		}
	}
}
