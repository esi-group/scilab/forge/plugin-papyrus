// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.eclipse.papyrus.agesyscilab.actions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Properties;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import org.scilab.modules.xcos.addon.rmi.ComputeInEclipse;
import org.scilab.modules.xcos.addon.rmi.ComputeInXcos;
import static org.eclipse.papyrus.agesyscilab.Activator.log;


public class ConnectionManager extends Thread
{
	/**
	 * Constants
	 */
	private static final String POLICY_FILE = "client.policy";
	private static final int SLEEP_PERIOD_IN_MS = 500;
	
	
	/**
	 * Attributes
	 */
	private String policy;
	public static boolean rmiRegistrationDone;
	public static ComputeInXcos comp;
	public boolean exit_now;
	
	
	/**
	 * Constructor
	 */
	public ConnectionManager ()
	{
		policy = null;
		rmiRegistrationDone = false;
		comp = null;
		exit_now = false;
	}
	
	
	/**
	 * Private functions
	 */
	
	/**
	 * Copy to the java temporary folder a file
	 * 
	 * @param url : the url of the file to copy
	 * @param name : the name of the temporary file to create
	 * @param extension : the extention of the temporaty file to create
	 * @return the absolute path of the created temporary file
	 * @throws Exception
	 */
	private String createFileOnTemp (URL url, String name, String extension) throws Exception
	{
		try 
		{
			File fo = File.createTempFile(name, extension);
			fo.deleteOnExit();
			
			FileOutputStream fos = new FileOutputStream(fo);
			InputStream is = url.openStream();
			
			final byte buffer[] = new byte[256 * 1024];
			int nbLecture = 0; 
			while ((nbLecture = is.read(buffer)) != -1) 
			{
				fos.write(buffer, 0, nbLecture);
			}
			fos.close();
			
			return fo.getAbsolutePath();
		} catch (Exception e)
		{
			log.error(e);
			return null;
		}
	}
	
	/**
	 * store in policy attribute the url of the policy file used in java RMI mecanism
	 * 
	 * @throws Exception
	 */
	private void getCodebaseAndPolicyFilePath () throws Exception
	{
		Bundle bundle = Platform.getBundle(org.eclipse.papyrus.agesyscilab.Activator.PLUGIN_ID);
		Path path_policy = new Path(POLICY_FILE);
		URL policy_url = FileLocator.find(bundle, path_policy, null);
		
		try {
			// resolving policy file path
			policy_url = FileLocator.resolve(policy_url);
			String policy_path = policy_url.toString();
			if (policy_path.indexOf("!") > -1)
			{
				// copy the client.policy file into the TMP directory and point policy variable to it
				policy = createFileOnTemp(policy_url, "client", ".policy");
				policy = "file://" + policy;
			} else
			{
				policy = policy_path;
			}
		} catch (Exception e)
		{
			log.error(e);
		}
	}
	
	/**
	 * Register to the java RMI mecanism :
	 * 	- an interface to receive data from external java application
	 *  - an interface to send to an external java application some data
	 * 
	 * @throws Exception
	 */
	private void registerRmi () throws Exception
	{
		Properties prop_policy = new Properties(System.getProperties());
		String value_policy = policy;
		prop_policy.setProperty("java.security.policy", value_policy);
		System.setProperties(prop_policy);

		// For sending action from eclipse to Scilab
		String name = "ComputeInXcos";
		Registry registry = LocateRegistry.getRegistry("localhost");
		comp = (ComputeInXcos) registry.lookup(name);

		// For recieving action from Scilab to eclipse
		String toEclipse = "ComputeInEclipse";
		ComputeInEclipse engine = new PluginAction();
		ComputeInEclipse stub = (ComputeInEclipse) UnicastRemoteObject.exportObject(engine, 0);
		registry.rebind(toEclipse, stub);
	}
	
	/**
	 * Public functions
	 */
	
	/**
	 * Thread public function
	 */
	public void run ()
	{
		try 
		{
			while (exit_now == false)
			{
				try 
				{
					if (rmiRegistrationDone == false)
					{
						// get Scilab toolbox path
						getCodebaseAndPolicyFilePath();

						// registering to java rmi
						registerRmi();
						log.info("\t- Java Rmi registered");

						rmiRegistrationDone = true;
					}
				} catch (Exception e)
				{
					rmiRegistrationDone = false;
					log.error(e);
				}

				Thread.sleep(SLEEP_PERIOD_IN_MS);
			}
		} catch (Exception e)
		{
			log.error(e);
		}
	}
}
