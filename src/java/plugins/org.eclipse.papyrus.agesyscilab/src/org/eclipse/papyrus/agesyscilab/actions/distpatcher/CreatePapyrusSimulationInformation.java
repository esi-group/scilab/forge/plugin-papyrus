// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.eclipse.papyrus.agesyscilab.actions.distpatcher;

import java.util.ArrayList;

import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.infra.core.utils.ServiceUtils;
import org.eclipse.papyrus.uml.service.types.element.UMLElementTypes;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.NamedElement;

import org.eclipse.papyrus.agesyscilab.actions.*;

import static org.eclipse.papyrus.agesyscilab.Activator.log;


public class CreatePapyrusSimulationInformation
{
	/**
	 * Constants
	 */
	public static final String INFORMATION_SOURCE = "information";
	public static final String UID_SOURCE = "uid";
	public final static String SUPERBLOCK_NAME = "SUPER_f";
	
	
	/**
	 * Attributes
	 */
	
	
	/**
	 * Constructors
	 */
	
	
	/**
	 * Private functions
	 */
	
	/**
	 * Store a String information in an EMF object.
	 * EAnnotation are used.
	 * 
	 * @param element : the containing EMF object
	 * @param value : the String value to set
	 * @param eannotation_source : the category of EAnnotation
	 */
	private static void createInformation(final NamedElement element, final String value, final String eannotation_source)
	{
		final Comment c = (Comment) PapyrusLibrary.createSysmlElementFromUmlType(element, UMLElementTypes.COMMENT, null);
		
		try 
		{	
			TransactionalEditingDomain editingDomain = ServiceUtils.getInstance().getTransactionalEditingDomain(PapyrusLibrary.getServiceRegistry());
			CommandStack stack = editingDomain.getCommandStack();
			RecordingCommand rc = new RecordingCommand(editingDomain)
			{
				@Override
				protected void doExecute() {
					c.setBody(value);
					EAnnotation ea = element.createEAnnotation(eannotation_source);
					ea.getContents().add(c);
					element.getEAnnotations().add(ea);
				}
			};
			stack.execute(rc);
		} catch (Exception e)
		{
			log.error(e);
		}
	}
	
	/**
	 * Add some simulation information to an EMF activity object
	 * 
	 * @param activity : the containing activity
	 * @param information : the information to store inside the activity
	 */
	private static void addSimulationInformation(Activity activity, Object information)
	{
		String value = null;
		
		if (information instanceof String)
		{
			value = (String) information;
		} else if (information instanceof Double)
		{
			double d = (Double) information;
			value = Double.toString(d);
		} else if (information instanceof Integer)
		{
			int i = (Integer) information;
			value = Integer.toString(i);
		} else if (information instanceof String[])
		{
			String [] string_array = (String []) information;
			for (String str : string_array)
			{
				value = value + str + "\n";
			}
		} else
		{
			log.info("\tThe given information is not managed");
		}
		
		createInformation(activity, value, INFORMATION_SOURCE);
	}
	
	/**
	 * Store a list of information in an EMF activity
	 * 
	 * @param activity
	 * @param information_list
	 */
	private static void createSimulationInformation (Activity activity, ArrayList<Object> information_list)
	{
		for (Object information : information_list)
		{
			addSimulationInformation(activity, information);
		}
	}
	
	/**
	 * Create an activity if it does not exists,
	 * store the simulation information inside
	 * and finally create the corresponding activity diagram
	 * to this activity
	 * 
	 * @param parameters : the parameters to deal with
	 */
	private static void createSysmlItem(ArrayList<Object> parameters)
	{
		log.info("--> CreatePapyrusSimulationInformation");
		
		try 
		{
			// remove diagram name and uid from parameters
			String diagram_name = (String) parameters.get(0);
			parameters.remove(0);
			String diagram_uid = (String) parameters.get(0);
			parameters.remove(0);
			
			// get an activity container if exists
			NamedElement container = PapyrusLibrary.getElementByUid(null, diagram_uid);

			if (container instanceof org.eclipse.uml2.uml.Package)
			{
				// create main activity
				Activity root_activity = (Activity) PapyrusLibrary.createSysmlElementFromUmlType(container, UMLElementTypes.ACTIVITY, null);
				PapyrusLibrary.modifyNameFromNamedElement(root_activity, diagram_name);

				// assign to the main activity its diagram uid
				createInformation(root_activity, diagram_uid, UID_SOURCE);

				// map simulation information
				createSimulationInformation(root_activity, parameters);
			}
			
			// create an activity diagram which container is described by the diagram_uid

			ArrayList<Object> diagram_parameters = new ArrayList<Object> ();
			diagram_parameters.add(diagram_name);
			diagram_parameters.add(diagram_uid);
			CreatePapyrusActivityDiagram.executeTask(diagram_parameters);
			
		} catch (Exception e)
		{
			log.error(e);
		}
		
		log.info("<-- CreatePapyrusSimulationInformation\n");
	}

	/**
	 * Public functions
	 */
	
	/**
	 * The main action of this class
	 * 
	 * @param parameters : the parameters to deal with
	 * @return
	 */
	public static ArrayList<Object> executeTask(final ArrayList<Object> parameters)
	{
		Display.getDefault().syncExec(
			new Runnable() 
			{
				public void run()
				{ 
					createSysmlItem(parameters);
				} 
			}
		);
		
		return null;
	}
}
