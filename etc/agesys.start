// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

function [agesyslib] = startModule()

  // Check Scilab's version
  // =============================================================================
  try
	  v = getversion("scilab");
  catch
	  error(gettext("Scilab 5.5 or more is required."));
  end

  if v(2) < 5 then
	  // new API in scilab 5.5
	  error(gettext('Scilab 5.5 or more is required.'));  
  end

  mprintf("Start Agesys\n");
  if isdef("agesyslib") then
    warning("Agesys toolbox is already loaded");
    return;
  end

  etc_tlbx  = get_absolute_file_path("agesys.start");
  etc_tlbx  = getshortpathname(etc_tlbx);
  root_tlbx = strncpy( etc_tlbx, length(etc_tlbx) - length("\etc\"));

  //Load  functions libraries
  // =============================================================================
  mprintf("\tLoad macros\n");

  pathmacros_common     = pathconvert( root_tlbx ) + "macros" + filesep();
  agesyslib             = lib(pathmacros_common);

  // load java
  // =============================================================================
  mprintf("\tLoad jar\n");
  // load Xcos needed jars in classpath
  loadXcos();

  // load gateways
  // =============================================================================
  mprintf("\tLoad gateways\n");
  verbose_level = ilib_verbose();
  ilib_verbose(0);
  exec(pathconvert(root_tlbx+"/sci_gateway/loader_gateway.sce",%f));
  exec(pathconvert(root_tlbx+"/src/loader_src.sce",%f));
  ilib_verbose(verbose_level);

  // Add help chapter
  // =============================================================================
  if or(getscilabmode() == ["NW";"STD"]) then
    mprintf("\tLoad help\n");
    path_addchapter = pathconvert(root_tlbx+"/jar");
    if ( isdir(path_addchapter) <> [] ) then
      add_help_chapter("agesys", path_addchapter, %F);
    end
  end

  // Add demos
  // =============================================================================
  if or(getscilabmode() == ["NW";"STD"]) then
    mprintf("\tLoad demos\n");
    pathdemos = pathconvert(root_tlbx + "/demos/agesys.dem.gateway.sce", %f, %t);
    add_demo(gettext("Agesys"),pathdemos);
  end

  // register rmi
  // ============================================================================
  mprintf("\tRegister with java rmi\n");
  rmi_register();

  // customize Xcos Tools menu
  // ============================================================================
  mprintf("\tAdd an Xcos menu entry\n");
  xcosAddToolsMenu("Sync back to Eclipse", "rmi_sync()");

endfunction

// =============================================================================
[agesyslib] = startModule();
clear startModule;
// =============================================================================
